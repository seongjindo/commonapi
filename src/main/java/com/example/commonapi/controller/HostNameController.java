package com.example.commonapi.controller;

import com.example.commonapi.response.BasicResponse;
import com.example.commonapi.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;

@RestController
@Slf4j
public class HostNameController {

    private final HttpStatus httpStatus = HttpStatus.OK;

    @GetMapping(value="/hostname")
    public ResponseEntity<? extends BasicResponse> hostname() throws Exception{
        String hostName = InetAddress.getLocalHost().getHostName();
        System.out.println(hostName);
        return new ResponseEntity<>(new CommonResponse<>(hostName), httpStatus);
    }
}
