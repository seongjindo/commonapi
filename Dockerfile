FROM eclipse-temurin:21.0.1_12-jdk-alpine
COPY build/libs/*.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "java -jar /app.jar"]